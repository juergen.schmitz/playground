#!/bin/bash

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters."
    echo "usage: $0 startDate endDate desc"
    echo "date format: yyyymmdd"
    exit 1
fi

#echo "param $1, param $2"

END=$(date -d "$2" +%s);
DATE=$(date -d "$1" +%s); 
while [[ "$DATE" -le "$END" ]]; do 
    DAY=$(date -d "@$DATE" +%Y%m%d)
    echo "$DAY $3"
    let DATE+=86400 
done
