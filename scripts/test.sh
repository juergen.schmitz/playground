#!/bin/bash

VAR="VAR"
NEW="${VAR}_sgg77vm"
# echo ${!NEW}
# NEW="test"

echo value of the variable: ${NEW} is \'${!NEW}\'
echo try to change the value of ${NEW}.
# unset ${!NEW}
# set ${!NEW}="changed value"
# ${!NEW}="changed value"

eval "$NEW='test'"
echo new value of the variable: ${NEW} is \'${!NEW}\'
echo check if variable: VAR_sgg77vm was actually: ${VAR_sgg77vm}
