#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters."
    echo "Usage: $0 srcCalFolder desJsonVacation "
    echo "Creates from all files in srcCalFolder a JsonConfigFile"
    exit 1
fi

FILES=$1
OUT_JSON_FILE=$2
c=0
COLORS=(black red green blue magenta "#023e7d" "#E8C2CA" "#5A189A" "#9D4EDD")

echo "var vacations = [" > $OUT_JSON_FILE

#iterate over all files in cal and create pal calendar files
for calFile in $FILES
do
    #create pal calendar file
    echo $calFile
    title=$(echo $calFile | cut -d '/' -f 2)

    cat $calFile | while read line || [[ -n $line ]];
    do
        start=$(echo $line | cut -d ' ' -f 1)
        end=$(echo $line | cut -d ' ' -f 2)
        color=${COLORS[$c]}

        echo "   {"                       >> $OUT_JSON_FILE
        echo "      title: '"$title"',"   >> $OUT_JSON_FILE
        echo "      start: '"$start"',"   >> $OUT_JSON_FILE
        echo "      end:   '"$end"',"     >> $OUT_JSON_FILE
        echo "      color: '"$color"',"   >> $OUT_JSON_FILE
        echo "  },"                       >> $OUT_JSON_FILE

    done
    
    #next color index
    if [ "$c" -lt 9 ]; then
        let "c=c+1"
    else
        c=0
    fi
done

echo "];" >> $OUT_JSON_FILE