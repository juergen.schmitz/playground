#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters."
    echo "usage: $0 pal.conf"
    echo "Creates a config file for a pal calendar"
    exit 1
fi


PAL_CONF_BASE=pal-base.conf
PAL_CONF=$1
FILES=../cal/*

COLORS=(black red green yellow blue magenta cyan white)
c=0

#create base content of pal calendar config
cat $PAL_CONF_BASE > $PAL_CONF

#iterate over all files in cal and create pal calendar files
for f in $FILES
do
    #create pal calendar file
    NAME=$(echo $f | cut -d '/' -f 3)
    PAL_NAME="$NAME.pal"
    echo "XX $NAME" > $PAL_NAME
    cat $f | while read line || [[ -n $line ]];
    do
        ./printDays.sh $line $NAME >> $PAL_NAME
    done
    
    #add pal calender to config file
    echo "file $PAL_NAME (${COLORS[$c]})" >> $PAL_CONF
    if [ "$c" -lt 7 ]; then
        let "c=c+1"
    else
        c=0
    fi
done

