#!/bin/bash
#creates from cal folder an html calendar and stores it in index.html

PAL_CONF=pal.conf
OUTPUT_PAGE=$1

./createPalsFromCalFolder.sh $PAL_CONF

mkdir tmp
cd tmp
pal -f ../$PAL_CONF -c 12 --latex > index.tex
htlatex ./index.tex
mv index.html ../$OUTPUT_PAGE
cd ..
rm -rf tmp
rm $PAL_CONF
rm *.pal
