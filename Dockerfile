FROM debian
WORKDIR /home/

RUN  apt update && apt -y install \
pal \
texlive-plain-generic \
tex4ht \
texlive-latex-base \
texlive-latex-extra \
texlive-latex-recommended
