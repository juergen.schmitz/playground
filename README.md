# [Vacation calendar](https://juergen.schmitz.gitlab.io/playground)
This repo creates web based calendar. This calendar is used to get an overview about the vacation days of the team members.

To add a vaction to the calendar, simply add a file with your name to the folder: ```cal```. Remark the file name will be used as the title of the calendar entry.

The content of the file has to contain the start and the end date of the vacation in one line. Each vacation (start/end date) goes into an own line.
```
2021-01-04 2021-01-08 
2021-02-08 2021-02-12
``` 

## Technical details
For creating the calendar the linux tool ```pal``` is used. This exports the calendar as an latex document. This document is then finally converted to the an ```html``` page. This html page is stored in public folder and made available as an artifact see ```gitlab.yaml```. This artifact is then used by the [gitlab pages feature](https://juergen.schmitz.gitlab.io/playground) to hosts static html pages.

* ```Dockerfile``` - used to create a customized ruby image containing pal and jekyll
* ```public/index.css``` - defines the style sheets for the calendar
* ```./scripts/pal-base.conf``` - contains the config for the pal calendar
* ```./scripts/printDays.sh``` - helper script used to calculates the dates from the vacation time-spanes defined in the cal folder
* ```./scripts/createPalsFromCalFolder.sh``` - creates pal calendar files with the help of the printDays.sh
* ```./scripts/createCalendar.sh``` - creates with the help of ```pal``` calendar and the ```htlatex``` a html page representing the calendar.


